# TFM

This repository is the initial one on the group called TFM, under the url: https://gitlab.com/tfm_3dprinting_sensors. Here the steps to configure a ROS workspace to capture data from an Arduino UNO board and control a differential wheeled robot are described. This is part of my master's thesis about 3d printing force sensors for biomedical applications via FDM/FFF 3d printing technology.

## Prerequisites

This repository assumes you use `Ubuntu 20.04 LTS` and `ROS noetic`. If you want to use VirtualBox, please follow the additional steps described.

#### 0. Configure VirutalBox

- Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and configure a machine with `Ubuntu 20.04 LTS`.
- Configure Arduino USB port on VirtualBox machine. Before running it, go to your virtual machine at **Settings > USB** and add your Arduino Board (which must be connected).

#### 1. Install ROS

- Install ROS noetic following the steps at: http://wiki.ros.org/noetic/Installation/Ubuntu.

#### 2. Install dependencies
```
sudo apt update
sudo apt install python3 python3-catkin-tools ros-noetic-control-toolbox
```

#### 3. Configure Arduino IDE

- Install the [Arduino IDE](https://www.arduino.cc/en/Guide/Linux).

- Install rosserial package:
```
sudo apt install ros-noetic-rosserial
sudo apt install ros-noetic-rosserial-arduino
```
- Open Arduino IDE, navigate to **Sketch > Include Library > Manage Library** and search the rosserial package to install it.

#### 4. Init workspace

- Create a ROS workspace for the project:
```
source /opt/ros/noetic/setup.bash

mkdir -p tfm_ws/src

cd tfm_ws

catkin config --extend /opt/ros/noetic
```

## Set up workspace

### Download the workspace configuration file

Check out the project repository into your workspace:

```
cd /home/<USER>/tfm_ws/src
git clone https://gitlab.com/tfm_3dprinting_sensors/tfm_workspace_setup.git

```

### Import workspace settings

Impor the config file: (e.g. /home/<USER>/tfm_ws/src)

```
vcs import < tfm_workspace_setup/tfm_workspace_setup.yaml --recursive
```

Generic command: `vcs import < ros_workspace_setup/<config>.yaml  --recursive.`

### Build

- Compile the workspace (e.g. /home/<USER>tfm_ws):
```
catkin build
```
- Source the new workspace (e.g. /home/<USER>tfm_ws):
```
echo "source ${PWD}/devel/setup.bash" >> ~/.bashrc
```
- Reopen your terminal and type:
```
roscd
```

### Run

To run with the real hardware:

- Build and upload the Arduino code to the board through the Arduino IDE.
- Open a terminal an bring up your ros nodes:
```
roslaunch wheelchair_bringup wheelchair.launch
```
```
rosrun tfm_perception_node tfm_perception_node  __name:=perception
```
- Open a new terminal and run the node on the Arduino board. Note that the serial port is determined at run time.
```
rosrun rosserial_python serial_node.py /dev/tty<USB# or ACM#>
```

To run only the simulations:
```
roslaunch diff_wheeled_robot_bringup diff_wheeled_robot_bringup.launch
```
```
rosrun tfm_perception_node tfm_perception_node __name:=perception
```
```
rosrun tfm_arduino_node_simulation tfm_arduino_node_simulation __name:=signal
```

<p><img width="300" src="./assets/motion.PNG" alt="diff_wheeled_robot_motion"></p>

## Useful commands

Execute the following commands in <workspace>/src.

### Pull latest code
```
vcs pull
```

## Authors and acknowledgment
Oriol Vendrell

## License
BSD 2-Clause "Simplified" License

## Project status
This project is part of my final master's thesis.

